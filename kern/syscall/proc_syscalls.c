#include <types.h>
#include <kern/errno.h>
#include <kern/unistd.h>
#include <kern/wait.h>
#include <lib.h>
#include <syscall.h>
#include <current.h>
#include <proc.h>
#include <thread.h>
#include <addrspace.h>
#include <copyinout.h>
#include <mips/trapframe.h>
#include <array.h>
#include <synch.h>
#include <kern/fcntl.h>
#include <vm.h>
#include <vfs.h>
#include <test.h>
#include "opt-A2.h"

  /* this implementation of sys__exit does not do anything with the exit code */
  /* this needs to be fixed to get exit() and waitpid() working properly */
void sys__exit(int exitcode) {

  struct addrspace *as;
  struct proc *p = curproc;
  /* for now, just include this to keep the compiler from complaining about
     an unused variable */
  (void)exitcode;

  DEBUG(DB_SYSCALL,"Syscall: _exit(%d)\n",exitcode);

  KASSERT(curproc->p_addrspace != NULL);
  	
  if (p->p_parent != NULL) {
    lock_acquire(p->p_parent->p_lk);
    p->p_exit_code = exitcode;
    p->p_is_alive = false;
    lock_release(p->p_parent->p_lk);
    cv_signal(p->p_cv, p->p_parent->p_lk);
  }

  as_deactivate();
  /*
   * clear p_addrspace before calling as_destroy. Otherwise if
   * as_destroy sleeps (which is quite possible) when we
   * come back we'll be calling as_activate on a
   * half-destroyed address space. This tends to be
   * messily fatal.
   */
  as = curproc_setas(NULL);
  as_destroy(as);

  /* detach this thread from its process */
  /* note: curproc cannot be used after this call */
  proc_remthread(curthread);

  /* if this is the last user process in the system, proc_destroy()
   will wake up the kernel menu thread */
  if (p->p_parent == NULL) {
    proc_destroy(p);
  }
  thread_exit();
  /* thread_exit() does not return, so we should never get here */
  panic("return from thread_exit in sys_exit\n");
}


/* stub handler for getpid() system call                */
int
sys_getpid(pid_t *retval)
{
  /* for now, this is just a stub that always returns a PID of 1 */
  /* you need to fix this to make it work properly */
  
  #if OPT_A2
	*retval = curproc->p_pid;
  #else
  	*retval = 1;
  #endif
  return(0);
}

/* stub handler for waitpid() system call                */

int
sys_waitpid(pid_t pid,
	    userptr_t status,
	    int options,
	    pid_t *retval)
{
  int exitstatus;
  int result;

  /* this is just a stub implementation that always reports an
     exit status of 0, regardless of the actual exit status of
     the specified process.   
     In fact, this will return 0 even if the specified process
     is still running, and even if it never existed in the first place.

     Fix this!
  */

  if (options != 0) {
    return(EINVAL);
  }
 
  int flag = 0;
  struct proc *p = curproc;
  
  lock_acquire(p->p_lk);
  for(unsigned int i=0; i<array_num(p->p_children); i++) {
    struct proc *child_p = array_get(p->p_children, i);
    if (child_p->p_pid == pid) {
      flag = 1;
      while(child_p->p_is_alive) {
	cv_wait(child_p->p_cv, p->p_lk);
      }
      exitstatus = _MKWAIT_EXIT(child_p->p_exit_code);
      array_remove(p->p_children, i);
      proc_destroy(child_p);
      break;
    }
  }  
  
  lock_release(p->p_lk);

  if(flag != 1) {
    *retval = -1;
    return ESRCH;
  }

  /* for now, just pretend the exitstatus is 0 */
//  exitstatus = 0;
//
  
  result = copyout((void *)&exitstatus,status,sizeof(int));
  if (result) {
    return(result);
  }
  *retval = pid;
  return(0);
}


/* stub handler for fork() system call                */

int sys_fork(struct trapframe *tf, pid_t *retval) {
  struct trapframe *tf_heap = kmalloc(sizeof(struct trapframe));
  if (tf_heap == NULL){
    DEBUG(DB_SYSCALL, "sys_fork error: could not create a new trapframe\n");
    return ENOMEM;
  }

  memcpy(tf_heap, tf, sizeof(struct trapframe));
	
  struct proc *child = proc_create_runprogram(curproc->p_name);
  if (child == NULL) {
    DEBUG(DB_SYSCALL, "sys_fork error: could not create a process.\n");
    kfree(tf_heap);
    return ENOMEM;
  }
  
  int err;
  
  DEBUG(DB_SYSCALL, "sys_fork: copying address space\n");
  
  spinlock_acquire(&child->p_lock);
  as_copy(curproc_getas(), &(child->p_addrspace));
  spinlock_release(&child->p_lock);
 
  if(child->p_addrspace == NULL) {
    DEBUG(DB_SYSCALL, "sys_fork error: could not copy the address space.\n");
    proc_destroy(child);
    kfree(tf_heap);
    return err;
  }
  DEBUG(DB_SYSCALL, "sys_fork: address space copied\n");  

  child->p_parent = curproc;
  array_add(curproc->p_children, child, NULL);
  
  err = thread_fork(curthread->t_name, child, &enter_forked_process, tf_heap, 0);
  
  if (err) {
    as_destroy(child->p_addrspace);
    proc_destroy(child);
    kfree(tf_heap);
    return err;
  }

  *retval = child->p_pid;

  return(0);

}

int sys_execv(userptr_t progname, userptr_t args) {
  struct addrspace *as, *old_as;
  struct vnode *v;
  vaddr_t entrypoint, stackptr;
  int result;
 
  size_t args_size;
  size_t prog_size = (strlen((char *)progname) + 1);
  int i = 0;
  int argc = 0;
  char **argk;
  char *prog_kernel = kmalloc(prog_size * sizeof(char));
  vaddr_t stackptr_t; 
  result = copyinstr((const_userptr_t) progname, prog_kernel, prog_size, NULL);
  
  if (result) {
    return result;
  }

  while(((char **)args)[i] != NULL) {
    argc++;
    i++;
  }
  
  args_size = (argc + 1) * sizeof(char *);
  argk = kmalloc(args_size);

  for(i=0; i<argc; i++) {
    size_t arg_size = (strlen(((char **)args)[i])+1);
    argk[i] = kmalloc(arg_size * sizeof(char));
    result = copyinstr((const_userptr_t)((char **)args)[i], argk[i] , arg_size, NULL);
    if (result) {
      return result;
    }
  }
  argk[argc] = NULL;
  if (result) {
    return result;
  }
  /* Open the file. */ 
  result = vfs_open(prog_kernel, O_RDONLY, 0, &v);
  if (result) {
    return result;
  }

  /* Create a new address space. */
  as = as_create();
  if (as ==NULL) {
    vfs_close(v);
    return ENOMEM;
  }

  /* Switch to it and activate it. */
  old_as = curproc_setas(as);
  as_activate();

  /* Load the executable. */
  result = load_elf(v, &entrypoint);
  if (result) {
    /* p_addrspace will go away when curproc is destroyed */
    vfs_close(v);
    return result;
  }
 
  /* Done with the file now. */
  vfs_close(v);  
  /* Define the user stack in the address space */
  result = as_define_stack(as, &stackptr);

  if (result) {
    /* p_addrspace will go away when curproc is destroyed */
    return result;
  }
 
  stackptr_t = stackptr;
  vaddr_t new_stack[argc+1];
 
  for (int i=argc-1; i>=0; i--) {
    size_t arg_size = ROUNDUP((strlen(argk[i]) + 1), 4);
    stackptr_t -= arg_size * sizeof(char);
    result = copyoutstr(argk[i], (userptr_t) stackptr_t, arg_size, NULL);
    if (result) {
      return result;
    }
    new_stack[i] = stackptr_t;
  }
  new_stack[argc] = (vaddr_t) NULL;

  for (int i=argc; i>=0; i--) {
    size_t arg_size = sizeof(vaddr_t);
    stackptr_t -= arg_size;
    result = copyout((void *)&new_stack[i], (userptr_t) stackptr_t, arg_size);
    if (result) {
      return result;
    }
  }

  as_destroy(old_as);
  kfree(prog_kernel);
  for (i=0; i<=argc; i++) {
    kfree(argk[i]);
  }
  kfree(argk);
 
  /* Warp to user mode. */
  enter_new_process(argc, (userptr_t) stackptr_t, ROUNDUP(stackptr_t, 8), entrypoint);
  /* enter_new_process does not return. */
  panic("enter_new_process returned\n");
  return EINVAL;
}
