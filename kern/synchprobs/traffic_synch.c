#include <types.h>
#include <lib.h>
#include <synchprobs.h>
#include <synch.h>
#include <opt-A1.h>
#include <array.h>

/* 
 * This simple default synchronization mechanism allows only vehicle at a time
 * into the intersection.   The intersectionSem is used as a a lock.
 * We use a semaphore rather than a lock so that this code will work even
 * before locks are implemented.
 */

/* 
 * Replace this default synchronization mechanism with your own (better) mechanism
 * needed for your solution.   Your mechanism may use any of the available synchronzation
 * primitives, e.g., semaphores, locks, condition variables.   You are also free to 
 * declare other global variables if your solution requires them.
 */

/*
 * replace this with declarations of any synchronization and other variables you need here
 */
static struct lock *intersectionLk;
static struct cv *cons[4];
int wait[4] = {0, 0, 0, 0};

volatile Direction pass;

volatile int count = 0;


/*
bool canPass(struct vehicle*, int);

bool canPass(struct vehicle *v, int c) {
  if((v->origin != origin) && (v->origin != destination || v->destination != origin) && (v->destination != destination || c!=3)) {
    return false
  }
  return true;
}
*/


/* 
 * The simulation driver will call this function once before starting
 * the simulation
 *
 * You can use it to initialize synchronization and other variables.
 * 
 */
void
intersection_sync_init(void)
{
  /* replace this default implementation with your own implementation */

  cons[0] = cv_create("north");
  cons[1] = cv_create("east");
  cons[2] = cv_create("south");
  cons[3] = cv_create("west");
 
  intersectionLk = lock_create("intersection");
  count = 0;
  
  pass = 0;

  if (intersectionLk == NULL) {
    panic("could not create intersection lock");
  }
  for(int i = 0; i <= 3; i++) {
    if (cons[i] == NULL) {
      panic("could not create cons cv");
    }
  }
  return;
}

/* 
 * The simulation driver will call this function once after
 * the simulation has finished
 *
 * You can use it to clean up any synchronization and other variables.
 *
 */
void
intersection_sync_cleanup(void)
{
  /* replace this default implementation with your own implementation */
  KASSERT(intersectionLk != NULL);
  lock_destroy(intersectionLk);
  
  for(int i=0; i<=3; i++) {
    KASSERT(cons[i] != NULL);
    cv_destroy(cons[i]);
  }
}


/*
 * The simulation driver will call this function each time a vehicle
 * tries to enter the intersection, before it enters.
 * This function should cause the calling simulation thread 
 * to block until it is OK for the vehicle to enter the intersection.
 *
 * parameters:
 *    * origin: the Direction from which the vehicle is arriving
 *    * destination: the Direction in which the vehicle is trying to go
 *
 * return value: none
 */

void
intersection_before_entry(Direction origin, Direction destination) 
{
  /* replace this default implementation with your own implementation */
  (void)origin;  /* avoid compiler complaint about unused parameter */
  (void)destination; /* avoid compiler complaint about unused parameter */
 
  // int cd = (destination - origin)%4;
  
  lock_acquire(intersectionLk);
  
  wait[origin]++;
  
  if ((wait[(origin+1)%4] > 0) || (wait[(origin+2)%4]>0) || (wait[(origin+3)%4]>0)) {
    cv_wait(cons[origin], intersectionLk);
  }

  if (count == 0) {
    pass = origin;
  }

  if((origin != pass) && (destination != pass || origin != (pass+1)%4)) {
    cv_wait(cons[origin], intersectionLk);
  }

  wait[origin]--;
  count++;

  lock_release(intersectionLk); 
}


/*
 * The simulation driver will call this function each time a vehicle
 * leaves the intersection.
 *
 * parameters:
 *    * origin: the Direction from which the vehicle arrived
 *    * destination: the Direction in which the vehicle is going
 *
 * return value: none
 */

void
intersection_after_exit(Direction origin, Direction destination) 
{
  /* replace this default implementation with your own implementation */
  (void)origin;  /* avoid compiler complaint about unused parameter */
  (void)destination; /* avoid compiler complaint about unused parameter */
  KASSERT(intersectionLk != NULL);
  
  lock_acquire(intersectionLk);

  count--;

  if (count == 0) {
    if (wait[(origin+1)%4] > 0) {
      pass = (origin+1)%4;
      cv_broadcast(cons[pass], intersectionLk);
    } else if (wait[(origin+2)%4] > 0) {
      pass = (origin+2)%4;
      cv_broadcast(cons[pass], intersectionLk);
    } else if (wait[(origin+3)%4] > 0) {   
      pass = (origin+3)%4;
      cv_broadcast(cons[pass], intersectionLk);
    } else if (wait[origin] > 0) {
      cv_broadcast(cons[origin], intersectionLk);
    }
  } 
  
  lock_release(intersectionLk);
}

